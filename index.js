// 1. Create a function that will add two numbers. Display the result ONLY in the console.

// Code #1

function addNumbers() {

	alert("This function will add two numbers.");
	let number1 = Number(prompt("Enter first number: "));
	let number2 = Number(prompt("Enter second number: "));
	let sum = number1 + number2;
	console.log("Your numbers are: " + number1 + " and " + number2);
	console.log("The sum of your two numbers is: " + sum);
}
addNumbers();

// 2. Create a function that will subtract two numbers. Display the result ONLY in the console. Use a prompt to get two values.

// Code #2

function subNumbers() {

	alert("This function will subtract two numbers.");
	let number3 = Number(prompt("Enter first number: "));
	let number4 = Number(prompt("Enter second number: "));
	let diff = number3 - number4;
	console.log("Your numbers are: " + number3 + " and " + number4);
	console.log("The difference of your two numbers is: " + diff);
}
subNumbers();

// 3. Create a function that will display a user's top 3 favorite movie characters. two numbers. Use a prompt to gather user input. Display the result ONLY in the console.

// Code #3

function topMovChar() {

	alert("What are your top 3 favorite movie characters?");
	let char1 = prompt("Enter your first fav movie character: ");
	let char2 = prompt("Enter your second fav movie character: ");
	let char3 = prompt("Enter your third fav movie character: ");
	
	console.log("Your favorite movie characters are:");
	console.log("1. " + char1);
	console.log("2. " + char2);
	console.log("3. " + char3);
}
topMovChar();

// 4. Create a function that will calculate

// Code #4

function doggoYears() {

	alert("How old is your dog?");
	let dogYear = prompt("Enter your dog's age: ");
	let doggoYear = Number(dogYear) * 7;
	console.log("You entered " + dogYear + " for your dog.")
	console.log("Your doggo is " + doggoYear + " in human years!");

}
doggoYears();

// 5. Create a function that takes 3 subject grades

// Code #5

function subAve() {

	alert("This will calculate average of your 3 subjects.");
	let math = Number(prompt("Enter your math grade: "));
	let science = Number(prompt("Enter your science grade: "));
	let english = Number(prompt("Enter your english grade: "));
	let result = (math + science + english) / 3;
	console.log("Your math grade is: " + math);
	console.log("Your science grade is: " + science);
	console.log("Your english grade is: " + english);
	console.log("Your average is " + result);

}
subAve();










